<?php

use App\Models\Course;
use App\Models\User;

it('has courses', function () {
    $user = User::factory()
        ->has(Course::factory()->count(2), 'purchasedCourses')
        ->create();

    expect($user->purchasedCourses)
        ->toHaveCount(2)
        ->each
        ->toBeInstanceOf(Course::class);
});
