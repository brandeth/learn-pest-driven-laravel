<?php

use App\Livewire\VideoPlayer;
use App\Models\Course;
use App\Models\User;
use App\Models\Video;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Livewire\Livewire;

beforeEach(function () {
    $this->loggedInUser = loginAsUser();
});

function createCourseAndVideos(int $videosCount = 1, array|Sequence $videoState = [])
{
    return Course::factory()
        ->has(Video::factory()
            ->count($videosCount)
            ->state($videoState))
        ->create();
}

it('shows details from given video', function () {
    $course = createCourseAndVideos();

    $video = $course->videos()->first();

    Livewire::test(VideoPlayer::class, ['video' => $video])
        ->assertSeeText([
            $video->title,
            $video->description,
            "({$video->duration_in_min}min)",
        ]);
});

it('shows given video', function () {
    $course = createCourseAndVideos();

    $video = $course->videos()->first();

    Livewire::test(VideoPlayer::class, ['video' => $video])
        ->assertSeeHtml('<iframe src="https://player.vimeo.com/video/' . $video->vimeo_id . '"');
});

it('shows list of all course videos', function () {
    $course = createCourseAndVideos(videosCount: 3);

    Livewire::test(VideoPlayer::class, ['video' => $course->videos()->first()])
        ->assertSee([
            ...$course->videos->pluck('title')->toArray(),
        ])
        ->assertSeeHtml([
            route('pages.videos', [$course, $course->videos[1]]),
            route('pages.videos', [$course, $course->videos[2]]),
        ]);
});

it('does not include route for current video', function () {
    $course = createCourseAndVideos();

    Livewire::test(VideoPlayer::class, ['video' => $course->videos()->first()])
        ->assertDontSeeHtml([
            route('pages.videos', $course->videos()->first()),
            route('pages.videos', [$course, Video::where('title', 'First Video')->first()]),
            route('pages.videos', [$course, Video::where('title', 'Second Video')->first()]),
            route('pages.videos', [$course, Video::where('title', 'Third Video')->first()]),
        ]);
});

it('marks video as completed', function () {
    $course = createCourseAndVideos();

    $this->loggedInUser->purchasedCourses()->attach($course);

    expect($this->loggedInUser->watchedVideos)->toHaveCount(0);

    Livewire::test(VideoPlayer::class, ['video' => $course->videos()->first()])
        ->call('markVideoAsCompleted');

    $this->loggedInUser->refresh();
    expect($this->loggedInUser->watchedVideos)
        ->toHaveCount(1)
        ->first()->title->toEqual($course->videos()->first()->title);
});

it('marks video as not completed', function () {
    $course = createCourseAndVideos();

    $this->loggedInUser->purchasedCourses()->attach($course);
    $this->loggedInUser->watchedVideos()->attach($course->videos()->first());

    expect($this->loggedInUser->watchedVideos)->toHaveCount(1);

    loginAsUser($this->loggedInUser);
    Livewire::test(VideoPlayer::class, ['video' => $course->videos()->first()])
        ->call('markVideoAsNotCompleted');

    $this->loggedInUser->refresh();
    expect($this->loggedInUser->watchedVideos)
        ->toHaveCount(0);
});
