<?php

use App\Livewire\VideoPlayer;
use App\Models\Course;
use App\Models\Video;
use Illuminate\Database\Eloquent\Factories\Sequence;

use function Pest\Laravel\get;

it('cannot be accessed by guest', function () {
    $course = Course::factory()
        ->has(Video::factory())
        ->create();

    get(route('pages.videos', $course))
        ->assertRedirect(route('login'));
});

it('includes video player', function () {
    $course = Course::factory()
        ->has(Video::factory())
        ->create();

    loginAsUser();
    get(route('pages.videos', $course))
        ->assertOk()
        ->assertSeeLivewire(VideoPlayer::class);
});

it('show first course video by default', function () {
    $course = Course::factory()
        ->has(Video::factory()->count(2))
        ->create();

    loginAsUser();
    get(route('pages.videos', $course))
        ->assertOk()
        ->assertSeeText("{$course->videos->first()->title}");
});

it('shows provided course video', function () {
    $course = Course::factory()
        ->has(
            Video::factory()
                ->state(new Sequence(['title' => 'First video'], ['title' => 'Second video']))
                ->count(2)
        )
        ->create();

    loginAsUser();
    get(route('pages.videos', ['course' => $course, 'video' => $course->videos()->orderByDesc('id')->first()]))
        ->assertOk()
        ->assertSeeText('Second video');
});
