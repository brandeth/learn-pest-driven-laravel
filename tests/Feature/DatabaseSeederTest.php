<?php

use App\Models\Course;

it('adds given courses', function () {
    $this->assertDatabaseCount(Course::class, 0);

    $this->artisan('db:seed');

    $this->assertDatabaseCount(Course::class, 3);
    $this->assertDatabaseHas(Course::class, ['title' => 'Laravel For Beginners']);
    $this->assertDatabaseHas(Course::class, ['title' => 'Advanced Laravel']);
});

it('adds given courses only once', function () {
    $this->artisan('db:seed');
    $this->artisan('db:seed');

    $this->assertDatabaseCount(Course::class, 3);
});

it('adds given videos', function () {
    $this->artisan('db:seed');
    $laravelForBeginnersCourse = Course::where('title', 'Laravel For Beginners')->first();
    $advancedLaravelCourse = Course::where('title', 'Advanced Laravel')->first();
    $tddTheLaravelWayCourse = Course::where('title', 'TDD The Laravel Way')->firstOrFail();

    expect($laravelForBeginnersCourse)
        ->videos
        ->toHaveCount(3);

    expect($advancedLaravelCourse)
        ->videos
        ->toHaveCount(3);

    expect($tddTheLaravelWayCourse)
        ->videos
        ->toHaveCount(2);
});

it('adds given videos only once', function () {
    $this->artisan('db:seed');
    $this->artisan('db:seed');
    $laravelForBeginnersCourse = Course::where('title', 'Laravel For Beginners')->first();
    $advancedLaravelCourse = Course::where('title', 'Advanced Laravel')->first();

    expect($laravelForBeginnersCourse)
        ->videos
        ->toHaveCount(3);

    expect($advancedLaravelCourse)
        ->videos
        ->toHaveCount(3);
});
